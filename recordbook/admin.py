from django.contrib import admin
from .models import RecordBook

# Register your models here.
admin.site.register(RecordBook)