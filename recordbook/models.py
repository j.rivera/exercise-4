from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models


class RecordBook(models.Model):
	fname = models.CharField(max_length=50)
	lname = models.CharField(max_length=50)
	contact = models.CharField(max_length=11, validators=[RegexValidator(r'^\d{1,11}$')], unique=True)
	address = models.CharField(max_length=100)
	userlog = models.ForeignKey('auth.User', null=True, on_delete=models.CASCADE)

	def __str__(self):
		return self.fname

	def __str__(self):
		return self.lname

	def __str__(self):
		return '%s %s' %(self.fname, self.lname)