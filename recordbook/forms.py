from django import forms
from .models import RecordBook

class RecordForm(forms.ModelForm):

	class Meta:
		model = RecordBook
		fields = (
			'fname', 'lname', 'contact', 'address',
			)
		labels = {
			"fname": "First Name :", "lname": "Last Name :",
			"contact": "Mobile Number :", "address": "Address :",
			}
			