from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.record_list, name='record_list'),
    path('records/', views.record_list, name='record_list'),

    # Register an account
    path('register/', views.register, name="register"),
    path('log-out/', views.log_out, name="log_out"),

    path('records/new/', views.addrecord, name='addrecord'),

    # Update a record
    path('records/update/<int:pk>/', views.update_record, name='update_record'),

    # Delete a record
    path('records/delete/<int:pk>/', views.delrecord, name='delrecord'),
    path('records/delete/confirm/<int:pk>/', views.confirm_delete, name='confirm_delete'),
]